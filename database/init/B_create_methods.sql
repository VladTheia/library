-- Procedures for adding data to tables

DELIMITER //
CREATE PROCEDURE addBook(bookName VARCHAR(50), authorId INT,
	genderId INT, availableNumber INT, totalNumber INT)
BEGIN
	INSERT INTO books (id, bookName, authorId, genderId, availableNumber, totalNumber)
		VALUES (NULL, bookName, authorId, genderId, availableNumber, totalNumber);
END
//

DELIMITER //
CREATE PROCEDURE addGender(name VARCHAR(20))
BEGIN
	INSERT INTO genders(id, genderName)
		VALUES (NULL, name);
END
//

DELIMITER //
CREATE PROCEDURE addAuthor(name VARCHAR(20))
BEGIN
	INSERT INTO authors (id, authorName) VALUES ((NULL), name);
END
//

DELIMITER //
CREATE PROCEDURE addUser(username VARCHAR(20), password VARCHAR(20), first_name VARCHAR(20),
	last_name VARCHAR(20), phone VARCHAR(20), email VARCHAR(20))
BEGIN
	INSERT INTO users (id, username, password, first_name, last_name, phone, email)
	VALUES ((NULL), username, password, first_name, last_name, phone, email);
END
//

DELIMITER //
CREATE PROCEDURE addLoan(user INT, book INT, start DATE, finish DATE)
BEGIN
	INSERT INTO loans(id, userId, bookid, startDate, endDate)
		VALUES (NULL, user, book, start, finish);
END
//

-- Procedure for searching books by filters

DELIMITER //
CREATE PROCEDURE getBooks(book VARCHAR(50), author VARCHAR(20), gender VARCHAR(20), available INT, user INT)
BEGIN
	SELECT b.id, b.bookName, a.authorName, g.genderName, b.availableNumber,
		CASE WHEN l.id IS NULL THEN 0 ELSE 1 END
	FROM books b
	JOIN authors a ON b.authorId = a.id
	JOIN genders g ON b.genderId = g.id
	LEFT JOIN loans l ON l.bookid = b.id AND l.userId = user AND l.endDate IS NULL
	WHERE (book IS NULL OR LOWER(b.bookName) LIKE LOWER(CONCAT('%', book, '%')))
		AND (author IS NULL OR LOWER(a.authorName) LIKE LOWER(CONCAT('%', author, '%')))
		AND (gender IS NULL OR LOWER(g.genderName) LIKE LOWER(CONCAT('%', gender, '%')))
		AND (available = 0 OR b.availableNumber > 0);
END
//

-- Procedure for borrowing a book

DELIMITER //
CREATE PROCEDURE borrowBookById(user INT, bookId INT)
BEGIN
	CALL addLoan(user, bookId, DATE(NOW()), NULL);
	COMMIT;
END
//

-- Procedure for returning a book

DELIMITER //
CREATE PROCEDURE returnBookById(user INT, bookId INT)
BEGIN
	UPDATE loans l SET l.endDate = DATE(NOW()) WHERE l.userId = user AND l.bookid = bookId;
	COMMIT;
END
//

-- Function for validating a user's credentials

DELIMITER //
CREATE FUNCTION checkCredentials(username VARCHAR(20), pass VARCHAR(20))
RETURNS INTEGER DETERMINISTIC
BEGIN
	DECLARE result INTEGER;
	SET result = (SELECT id FROM users WHERE username = username AND password = pass);
	IF result IS NULL
	THEN RETURN 0;
	END IF;
	RETURN result;
END
//

-- Procedure for creating the users loan report (most active users)

DELIMITER //
CREATE PROCEDURE reportUserLoans()
BEGIN
	SELECT u.username, u.first_name, u.last_name, COUNT(l.id) as counter
	FROM users u, loans l
	WHERE l.userId = u.id
	GROUP BY u.username, u.first_name, u.last_name
	ORDER BY counter DESC;
END
//

-- Procedure for creating the book loans report (most borrowed books)

DELIMITER //
CREATE PROCEDURE reportBookLoans()
BEGIN
	SELECT b.bookName, a.authorName, g.genderName, COUNT(l.id) as counter
	FROM books b, authors a, genders g, loans l
	WHERE b.authorId = a.id AND b.genderId = g.id AND l.bookId = b.id
	GROUP BY b.bookName, a.authorName, g.genderName
	ORDER BY counter DESC;
END
//

-- Triggers for automatically changing the number of available books

DELIMITER //
CREATE TRIGGER closeLoanTrigger AFTER UPDATE ON loans
FOR EACH ROW
BEGIN
	IF OLD.endDate IS NULL AND NEW.endDate IS NOT NULL
	THEN UPDATE books SET availableNumber = availableNumber + 1 WHERE id = NEW.bookId;
	END IF;
END
//

DELIMITER ;
CREATE TRIGGER addLoanTrigger AFTER INSERT ON loans
FOR EACH ROW
UPDATE books SET availableNumber = availableNumber - 1 WHERE id = NEW.bookId;
