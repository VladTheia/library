DELIMITER //
CREATE PROCEDURE populate()
BEGIN	
	CALL addGender('Comedie');
	CALL addGender('Romantic');
	CALL addGender('Science Fiction');
	CALL addGender('Politist');
	CALL addGender('Drama');

	CALL addAuthor('Ion Luca Caragiale');
	CALL addAuthor('Mihai Eminescu');
	CALL addAuthor('Isaac Asimov');
	CALL addAuthor('Agatha Christie');
	CALL addAuthor('Stephan King');
	
	CALL addBook('O scrisoare pierduta', 1, 1, 1, 100);
	CALL addBook('Poesii', 2, 2, 2, 20);
	CALL addBook('Imperiul', 3, 3, 3, 57);
	CALL addBook('10 negri mititei', 4, 4, 4, 49);
	CALL addBook('The Shining', 5, 5, 5, 10);
	
	CALL addUser('iulia', 'iulia', 'Iulia', 'Stefan', '0722243918', 'iulia.stefan');
	CALL addUser('renata', 'renata', 'Renata', 'Stefan', '0722243929', 'renata.stefan');
	CALL addUser('george', 'george', 'George', 'Popescu', '0724243306', 'george.popescu');
	CALL addUser('cristina', 'cristina', 'Cristina', 'Maria', '0722243507', 'cristina.maria');

	CALL addLoan(1, 2, '2019-01-10', NULL);
	CALL addLoan(3, 3, '2019-11-16', '2020-01-19');
	CALL addLoan(3, 3, '2019-09-08', NULL);
	CALL addLoan(4, 2, '2019-05-20', NULL);
END
//

DELIMITER ;
CALL populate;
