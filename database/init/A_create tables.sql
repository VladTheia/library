CREATE TABLE genders (
	id INT AUTO_INCREMENT primary key,
	genderName VARCHAR(20) NOT NULL,
	UNIQUE KEY (genderName)
);

CREATE TABLE authors (
	id INT AUTO_INCREMENT primary key,
	authorName VARCHAR(20) NOT NULL
);

CREATE TABLE books (
	id INT AUTO_INCREMENT primary key,
	bookName VARCHAR(50) NOT NULL,
	genderId INT NOT NULL,
	authorId INT NOT NULL,
	UNIQUE KEY (bookName, authorId),
	FOREIGN KEY (authorId) REFERENCES authors(id),
	FOREIGN key (genderId) REFERENCES genders(id),
	availableNumber INT,
	totalNumber INT
);

CREATE TABLE users (
	id INT AUTO_INCREMENT primary key,
	username VARCHAR(20) NOT NULL,
	password VARCHAR(20) NOT NULL,
	first_name VARCHAR(20) NOT NULL,
	last_name VARCHAR(20) NOT NULL,
	phone VARCHAR(20),
	email VARCHAR(20),
	UNIQUE KEY (username)
);

CREATE TABLE loans (
	id INT AUTO_INCREMENT primary key,
	userId INT NOT NULL,
	bookId INT NOT NULL,
	startDate DATE,
	endDate DATE,
	FOREIGN KEY (userId) REFERENCES users(id),
	FOREIGN KEY (bookId) REFERENCES books(id)
);
