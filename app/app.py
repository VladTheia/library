from datetime import datetime
from flask import Flask, request, session, render_template, redirect, url_for, g, flash
from flask_login import LoginManager, UserMixin, current_user, login_user, login_required, logout_user
import mysql.connector
from werkzeug.local import LocalProxy
from werkzeug.middleware.proxy_fix import ProxyFix


app = Flask(__name__)
app.config['SECRET_KEY'] = 'zJzNb5Zw8kJDRLWyszVu4NeZ2tPH8gkk'
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_host=1)

login_manager = LoginManager()
login_manager.init_app(app)

db_config = {
    'host': 'database',
    'port': '3306',
    'user': 'root',
    'password': 'root',
    'database': 'library'
}


@app.context_processor
def inject_now():
    return {'now': datetime.utcnow()}


def get_db():
    if 'db' not in g:
        g.db = mysql.connector.connect(**db_config)

    return g.db


db = LocalProxy(get_db)


@app.teardown_appcontext
def teardown_db(exception):
    db = g.pop('db', None)

    if db is not None:
        db.close()


class User(UserMixin):
    def __init__(self, id, username):
        self.id = id
        self.username = username

    def get_id(self):
        return self.username


@login_manager.user_loader
def load_user(username):
    user = None

    cursor = db.cursor()
    cursor.execute('SELECT id FROM users WHERE username = %s;', (username,))

    row = cursor.fetchone()
    if row:
        user = User(row[0], username)

    cursor.close()

    return user


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('index'))


@app.route('/')
def index():
    if not current_user.is_authenticated:
        return render_template('homepage.html')

    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    if request.method == 'GET':
        return render_template('login.html')

    form = request.form
    cursor = db.cursor()

    command = "SELECT checkCredentials(%s, %s);"
    arguments = (form['username'], form['password'])
    cursor.execute(command, arguments)
    user_id = cursor.fetchone()[0]

    if user_id:
        login_user(load_user(form['username']))
        response = redirect(url_for('index'))
    else:
        flash('Invalid username and password.', 'danger')
        response = render_template('login.html')

    cursor.close()

    return response


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if 'username' in session:
        return redirect(url_for('index'))

    if request.method == 'GET':
        return render_template('signup.html')

    cursor = db.cursor()
    form = request.form
    params = (
        form['username'], form['password'],
        form['firstName'], form['lastName'],
        form['phone'], form['email']
    )
    cursor.callproc('addUser', params)
    db.commit()
    cursor.close()
    login_user(load_user(form['username']))

    return redirect(url_for('index'))


@app.route('/logout')
@login_required
def logout():
    logout_user()

    return redirect(url_for('index'))


@app.route('/books')
@login_required
def books():
    args = request.args

    params = (
        args.get('book') or '',
        args.get('author') or '',
        args.get('genre') or '',
        1 if args.get('available') else 0,
        current_user.id
    )

    cursor = db.cursor()
    cursor.callproc("getBooks", params)
    res = []
    for result in cursor.stored_results():
        res += result.fetchall()
    cursor.close()

    return {'data': res}


@app.route('/borrow/<int:id>', methods=['POST'])
@login_required
def borrowBook(id):
    cursor = db.cursor()
    params = (current_user.id, id)
    cursor.callproc("borrowBookById", params)
    cursor.close()

    flash('Book borrowed!', 'success')

    return ''


@app.route('/return/<int:id>', methods=['POST'])
@login_required
def returnBook(id):
    cursor = db.cursor()
    params = (current_user.id, id)
    cursor.callproc("returnBookById", params)
    cursor.close()

    flash('Book returned!', 'success')

    return ''


@app.route('/loans/books')
@login_required
def bookLoansReport():
    cursor = db.cursor()
    cursor.callproc("reportBookLoans")
    res = []
    for result in cursor.stored_results():
        res += result.fetchall()
    cursor.close()

    return render_template('bookLoans.html', data=res)


@app.route('/loans/users')
@login_required
def userLoansReport():
    cursor = db.cursor()
    cursor.callproc("reportUserLoans")
    res = []
    for result in cursor.stored_results():
        res += result.fetchall()
    cursor.close()

    return render_template('userLoans.html', data=res)
