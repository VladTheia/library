$(() => {
    // GLobal elements
    let $searchForm = $('#searchForm'),
        $resultsTable = $('#resultsTable'),
        $bookLoansTable = $('#bookLoansTable'),
        $userLoansTable = $('#userLoansTable');

    // Functions
    let loadResults = () => {
        $resultsTable.DataTable().ajax.url(
            '/books?' +
            new URLSearchParams(new FormData($searchForm[0])).toString()
        ).load();
    };

    let getResultActions = (data) => {
        btn = [];
        if (data[5]) {
            btn.push('<a class="action bi-arrow-return-left" title="Return"></a>');
        } else if (data[4]) {
            btn.push('<a class="action bi-bookmark-fill" title="Borrow"></a>');
        }

        return btn.join('');
    };

    // DataTable init
    $resultsTable.DataTable({
        columns: [
            { data: 0 },
            { data: 1 },
            { data: 2 },
            { data: 3 },
            { data: 4 },
            {
                data: null,
                render: getResultActions
            }
        ]
    });
    $bookLoansTable.DataTable();
    $userLoansTable.DataTable();

    // Event listeners
    $resultsTable.find('tbody').on('click', 'a.action', function () {
        let data = $resultsTable.DataTable().row($(this).parents('tr')).data();

        if (data[5]) {
            $.post('/return/' + data[0]).always(() => {
                location.reload();
            })
        } else if (data[4]) {
            $.post('/borrow/' + data[0]).always(() => {
                location.reload();
            })
        }
    })

    $searchForm.submit((e) => {
        e.preventDefault();
        loadResults();
    });

    $('#searchFormReset').click(() => {
        $searchForm[0].reset();
        loadResults();
    });

    // Other initializations

    loadResults();
});
