up:
	docker-compose up -d

stop down:
	docker-compose $@

build:
	docker-compose build

.PHONY: up down stop build
