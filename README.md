# Library

## Description

A simple python app for managing a book library.

## Setup single node

You will need docker. To start the app, in the root of this project run:

```shell
make
```

Access the app in your browser at http://localhost/

## Setup Multiple nodes(Cluster)

### Docker machine
If you don't have a real cluster you can simulate this with docker machine.

First create at least 2 virtual machine.
```
docker-machine create --driver virtualbox myvm1
docker-machine create --driver virtualbox myvm2
```

Copy all files from local to one of the virtual-machine
```
docker-machine scp -r . myvm1:~/
```

Initiate docker swarm:
```
docker-machine ssh myvm1
myvm1 -> docker swarm init --advertise-addr $(IP)
myvm2 -> docker swarm join --token $(TOKEN) $(IP):$(PORT)
```

Start docker stack:
```
docker stack deploy -c docker-compose-cluster.yml Library
```

## Authors

* Iulia-Renata Ştefan
* Răzvan Neacșu
* Theia-Mădălin Vlad
* Viorel-Gabriel Mocanu
